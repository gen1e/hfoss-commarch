## I. The project's IRC Channel
  #jekyll on irc.freenode.net
## II. Source Code repository
 [https://github.com/jekyll/jekyll](https://github.com/jekyll/jekyll)
## III. Mail list archive
 N/A
## IV. Documentation
  [https://jekyllrb.com/docs/home](https://jekyllrb.com/docs/home)
## V. Other communication channels
  [https://talk.jekyllrb.com](https://talk.jekyllrb.com)

  [https://twitter.com/jekyylrb](https://twitter.com/jekyllrb)
## VI. Project Website and/or Blog
  [https://jekyllrb.com](https://jekyllrb.com)
## A.  Describe software project, its purpose and goals.
 Jekyll is a blogging framework used in github’s own blog page layout. Jekyll itself can be used to import any blog from any site into a Jekyll blog where markdown is supported as well as being simple and open source.
## B.  Give brief history of the project. When was the Initial Commit? The latest commit?
  The project started in October 19, 2008, with the first commit being for the start up of the git repo. The first version was made in 2010, v0.10.0. The last commit was March 19, 2018, and the last version to be released was v3.7.3 released on Febuary 25, 2018
## C.  Who approves patches? How many people?
 Teams in Jekyll. Teams are assigned by [@parkr](https://github.com/parkr), who manages the project overall, but he does not approve the patches directly. [@jekyllbot](https://github.com/jekyllbot) assignes a patch to a team, and that team then decides on approving the patch or not.
## D.  Who has commit access, or has had patches accepted?  How many total?
Commit access is owned solely by [@parkr](https://github.com/parkr) and [@jekyllbot](https://github.com/jekyllbot) currently at time of writing with a process going on behind the scenes for approval. Overall, 759 people have contributed to the project overall.
## E.  Who has the highest amounts of "Unique Knowledge?" (As per your "Git-by-a-bus" report. If there is a tie, list each contributor, with links if possible)
 Parker Moore AKA [@parkr](https://github.com/parkr), the lead developer and team organizer of the project. As seen in Image 1.
## F.  What is your project's "Calloway Coefficient of Fail?"
 65 - Kittens Die when your code is downloaded.
 This is caused by a lack of mailing list and the release version on their website containing a large amount of folders. However, installing by RubyGems gets rid of that issue bringing the score to 15 otherwise.
## G.  Has there been any turnover in the Core Team? (i.e. has the same top 20% of contributors stayed the same over time? If not, how has it changed?)
  Yes, some of the original members of the team have left including the original creator of the project. However, since switching over to a bot-based commit system where work is assigned to teams, it may be hard to gauge specifics, however, some PRs still show up by other members of the project.
## H.  Does the project have a BDFL, or Lead Developer? (BDFL == Benevolent Dictator for Life) If not, what is the structure of its leadership, and how is it chosen?
  [@parkr](https://github.com/parkr) is the Lead Developer currently, and through him, he assigns teams to focus on bug fixing and adding new features in distinct areas. The original developer was [@mojombo](https://github.com/mojombo) up until 2013 when he stopped contributing to the project.
## I.  Are the front and back end developers the same people? What is the proportion of each?
 Yes, because Jekyll naturally uses affinity teams to split up work, the build team are the ones focused entirely on the product code.
## J.  What have been some of the major bugs/problems/issues that have arisen during development? Who is responsible for quality control and bug repair?
 One of the biggest problems the team has faced is when they were transitioning to the team based assignments. In December of 2014, they all came together to decide the future of Jekyll, and even though it took some time to get there, the project is still moving and still as active. As far as quality control and bug repair, the community is the group supporting and fixing Jekyll. If there is an issue, the team responsible for where the bug came from will focus efforts on fixing said bug.
## K.  How is the project's participation trending and why?
 Since its inception, Jekyll has had a standard  amount of participation increasing heavily in 2013, and staying relatively flat throughout the lifespan of the project since.
## L.  In your opinion, does the project pass "The Raptor Test?" (i.e. Would the project survive if the BDFL, or most active contributor were eaten by a Velociraptor?) Why or why not?
 Yes. But maintenance would stall temporarily until a new leader took their place. The team based aspect of the project helps to spread the responsibility and work though gitbyabus cannot show this due to the hierarchy.
## M.  In your opinion, would the project survive if the core team, or most active 20% of contributors, were hit by a bus? Why or why not?
 159*.20=31 This top 31 would include the core team. The project may survive but may not be as actively maintained. Given how widely it is used, I expect that another group would pick up maintenance relatively quickly. They may not have the benefit of all the tools that the old team used but it would likely survive in some form.
## N.  Does the project have an official "on-boarding" process in place? (new contributor guides, quickstarts, communication leads who focus specifically on newbies, etc...)
 Yes, they have a support site located at [https://jekyllrb.com/docs/contributing/](https://jekyllrb.com/docs/contributing.) This site goes through how to install the most recent version, how to run tests, and how to get involved with the group, either by contributing to the discussion forum, or by helping via github or communicated via their irc.
## O.  Does the project have Documentation available? How extensive is it? Does it include code examples?
 Yes, the project’s documentation lives at [https://jekyllrb.com/docs/home/](htpps://jekyllrb.com/docs/home/). It is relatively extensive and includes many code examples and example blog outputs for use in Jekyll.
## P.  If you were going to contribute to this project, but ran into trouble or hit blockers, who would you contact, and how?
 They suggest contacting through their Jekyll discussion forum at [talk.jekyllrb.com](https://talk.jekyllrb.com) by either submitting a question or looking at some of the other people having the same kind of question.
## Q.  Based on these answers, how would you describe the decision making structure/process of this group?  Is it hierarchical, consensus building, ruled by a small group, barely contained chaos, or ruled by a single or pair of individuals?
 The decision making process appears to be hierarchical and ruled by a small group. The core development team and lead developer have final say on all changes but the process is delegated to teams for each portion of the project.
## R.  Is this the kind of structure you would enjoy working in? Why, or why not?
 Yes, although it would be nice to have less bureaucracy and red tape than this approach normally involves. It also depends on the attitudes of the community overall. If the core team is receptive to outside input then this model is much more consensus based.

## Appendix

### Image 1
Author Total Knowledge
![alt text](git_by_a_lion/author_tot_knowledge.svg "Author Domain Knowledge")

### Image 2
Author Total Risk
![alt text](git_by_a_lion/author_tot_risk.svg "Author Risk")

### Image 3
File Total Knowledge
![alt text](git_by_a_lion/file_tot_knowledge.svg "File per Knowledge")

### Image 4
File Total Risk
![alt text](git_by_a_lion/file_tot_risk.svg "File Risk")