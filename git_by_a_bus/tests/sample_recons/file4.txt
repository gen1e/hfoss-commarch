import sys
import os

from optparse import OptionParser
from subprocess import Popen
from string import Template

def exit_with_error(err):
    print >> sys.stderr, "Error: " + err
    exit(1)

def read_projects_file(fname, paths_projects):
    try:
        fil = open(fname, 'r')
        paths_projects.extend([line.strip() for line in fil if line.strip()])
        fil.close()
        return True
    except IOError:
        return False

def run_chained(cmd_ts, python_cmd, risk_file_option, bus_risk, departed_dev_option, output_dir):
    last_output_fname = None
    
    for cmd_t in cmd_ts:
        cmd_templates, output_fname = cmd_t
        
        output_f = None
        if output_fname:
            output_fname = os.path.join(output_dir, output_fname)
            if os.path.isfile(output_fname):
                last_output_fname = output_fname
                continue
            output_f = open(output_fname, 'w')

        stdin_f = None
        if last_output_fname:
            stdin_f = open(last_output_fname, 'r')
        
        if not isinstance(cmd_templates, list):
            cmd_templates = [cmd_templates]

        last_output_fname = output_fname

        for cmd_template in cmd_templates:
            cmd = Template(cmd_template).substitute(python_cmd=python_cmd,
                                                    risk_file_option=risk_file_option,
                                                    bus_risk=bus_risk,
                                                    departed_dev_option=departed_dev_option,
                                                    output_dir=output_dir).split(' ')
            cmd_p = Popen(cmd, stdin=stdin_f, stdout=output_f)
            cmd_p.communicate()

        if stdin_f:
            stdin_f.close()
        if output_f:
            output_f.close()

def main(paths_projects, options):
    # values for command subs...
    python_cmd = '/usr/bin/env python'

    output_dir = os.path.abspath(options.output or 'output')
    try:
        os.mkdir(output_dir)
    except:
        if not options.continue_last:
            exit_with_error("Output directory exists and you have not specified -c")

    risk_file_option = ''
    if options.risk_file:
        risk_file_option = '-r %s' % options.risk_file

    departed_dev_option = ''
    if options.departed_dev_file:
        departed_dev_option = '-d %s' % options.departed_dev_file

    # commands to chain together
    cmd_ts = []
    cmd_ts.append((['${python_cmd} gen_file_stats.py %s' % path_project for path_project in paths_projects], '__file_stats.txt'))
    cmd_ts.append(('${python_cmd} calc_file_knowledge_totals.py', '__file_knowledge_totals.txt'))
    cmd_ts.append(('${python_cmd} estimate_unique_knowledge.py ${departed_dev_option}', '__unique_knowledge.txt'))
    cmd_ts.append(('${python_cmd} estimate_file_risk.py -b ${bus_risk} ${risk_file_option}', '__estimated_risk.txt'))
    cmd_ts.append(('${python_cmd} summarize.py ${output_dir}', None))

    run_chained(cmd_ts, python_cmd, risk_file_option, options.bus_risk, departed_dev_option, output_dir)
    
if __name__ == '__main__':
    usage = """usage: %prog [options] [git_controlled_path1[=project_name1], git_controlled_path2[=project_name2],...]

               Analyze each git controlled path and create an html summary of orphaned / at-risk code knowledge.

               Paths must be absolute paths to local git-controlled directories (they may be subdirs in the git repo).
               
               Project names are optional and default to the last directory in the path.

               You may alternatively/additionally specify the list of paths/projects in a file with -p."""
    usage = '\n'.join([line.strip() for line in usage.split('\n')])

    parser = OptionParser(usage=usage)
    parser.add_option('-b', '--bus-risk', dest='bus_risk', metavar='FLOAT', default=0.1,
                      help='The default estimated probability that a dev will be hit by a bus in your analysis timeframe (defaults to 0.1)')
    parser.add_option('-r', '--risk-file', dest='risk_file', metavar='FILE',
                      help='File of dev=float lines (e.g. ejorgensen=0.4) with custom bus risks for devs')
    parser.add_option('-d', '--departed-dev-file', dest='departed_dev_file', metavar='FILE',
                      help='File listing departed devs, one per line')
    parser.add_option('-o', '--output', dest='output', metavar='DIRNAME', default='output',
                      help='Output directory for data files and html summary (defaults to "output"), error if already exists without -c')
    parser.add_option('-p', '--projects-file', dest='projects_file', metavar='FILE',
                      help='File of path[=project_name] lines, where path is an absoluate path to the git-controlled ' + \
                      'directory to analyze and project_name is the name to use in the output summary (project_name defaults to ' + \
                      'the last directory name in the path)')
    parser.add_option('-c', '--continue-last', dest='continue_last', default=False, action="store_true",
                      help="Continue last run, using existing output files and recreating missing.")

    options, paths_projects = parser.parse_args()

    if options.projects_file:
        if not read_projects_file(options.projects_file, paths_projects):
            exit_with_error("Could not read projects file %s" % options.projects_file)

    if not paths_projects:
        parser.error('No paths/projects!  You must either specify paths/projects on the command line and/or in a file with the -p option.')
    
    main(paths_projects, options)
