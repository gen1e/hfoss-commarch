diff --git a/git_by_a_bus.py b/git_by_a_bus.py
index 0794de1..7bdebb1 100644
--- a/git_by_a_bus.py
+++ b/git_by_a_bus.py
@@ -147,8 +147,7 @@ if __name__ == '__main__':
 
                Analyze each git controlled path and create an html summary of orphaned / at-risk code knowledge.
 
-               Paths must be absolute paths to local git-controlled directories (they may be subdirs in the git repo).  Currently
-               symlinked paths are not supported.
+               Paths must be absolute paths to local git-controlled directories (they may be subdirs in the git repo).
                
                Project names are optional and default to the last directory in the path.
 
